package isoft.tdd.datos.services;

import isoft.tdd.datos.model.Usuario;
import org.springframework.stereotype.Service;

@Service
public class UserService {


    private Usuario usuario;
    /*-----------------------|CREAR USUARIO|-----------------------------------*/
    public String crearUsuario(String nombre, String apellidoPaterno, String apellidoMaterno, String rut, String telefono, String edad){
        if (!validarTexto(nombre) || !validarTexto(apellidoPaterno) || !validarTexto(apellidoMaterno)){
            System.out.println("nombre inválido");
            return null;
        } else if (!validarLargoEdad(edad) || !validarValorEdad(edad)) {
            System.out.println("edad inválida");
            return null;
        } else if (!validarLargoRut(rut) || !digVerificadorRut(rut)) {
            System.out.println("rut inválido");
            return null;
        } else if (!validarLargoTelefono(telefono) || !validarTelefonoSinLetras(telefono)){
            System.out.println("telefono inválido");
            return null;
        }
        this.usuario = new Usuario(nombre, apellidoPaterno, apellidoMaterno, rut, telefono, edad);
        return "Usuario creado exitosamente";
    }

    public Usuario mostrarUsuario(){
        return this.usuario;
    }

    /*-----------------------|VALIDACIONES|-----------------------------------*/

    public boolean validarTexto(String texto){
        for (char caracter : texto.toCharArray()) {
            if (isNumero(caracter)) {
                return false;
            }
        }
        return true;
    }
    public boolean isNumero(char caracter){
        return Character.isDigit(caracter);
    }
    public boolean validarLargoRut(String rut){
        if (validarLargo(rut, 9, 10)) {
            return true;
        }
        return false;
    }

    public boolean validarLargoEdad(String edad){
        if (validarLargo(edad, 1, 2)){
            return true;
        }
        return false;
    }
    public boolean validarValorEdad(String edad){
        if (Integer.parseInt(edad) < 0 || Integer.parseInt(edad) > 130){
            return false;
        }
        return true;
    }

    public boolean validarLargoTelefono(String telefono){
        if (validarLargo(telefono, 9, 9)) {
            return true;
        }
        return false;
    }

    public boolean validarTelefonoSinLetras(String telefono){
        for (char caracter : telefono.toCharArray()) {
            if (!isNumero(caracter)) {
                return false;
            }
        }
        return true;
    }

    public boolean validarLargo(String cadena, int minimo , int maximo){
        return cadena.length() >= minimo && cadena.length() <= maximo;
    }

    public boolean digVerificadorRut(String rut){
        boolean validacion = false;
        try {
            rut =  rut.toUpperCase();
            rut = rut.replace(".", "");
            rut = rut.replace("-", "");
            int rutAux = Integer.parseInt(rut.substring(0, rut.length() - 1));

            char dv = rut.charAt(rut.length() - 1);

            int m = 0, s = 1;
            for (; rutAux != 0; rutAux /= 10) {
                s = (s + rutAux % 10 * (9 - m++ % 6)) % 11;
            }
            if (dv == (char) (s != 0 ? s + 47 : 75)) {
                validacion = true;
            }

        } catch (Exception ignored) {

        }
        return validacion;

    }



}
