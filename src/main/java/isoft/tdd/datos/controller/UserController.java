package isoft.tdd.datos.controller;

import isoft.tdd.datos.model.Usuario;
import isoft.tdd.datos.services.UserService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController("/api")
public class UserController {

    private UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping ("/crearUsuario")
    public String crearUsuario(String nombre, String apellidoPaterno, String apellidoMaterno, String rut, String telefono, String edad){
        return userService.crearUsuario(nombre, apellidoPaterno, apellidoMaterno, rut, telefono, edad);
    }
    @GetMapping("/mostrarUsuario")
    public Usuario mostrarUsuario(){
        return userService.mostrarUsuario();
    }

}
